//
//  Efecto.swift
//  playerV1
//
//  Created by Alvar Arias on 16/12/15.
//  Copyright © 2015 Alvar Arias. All rights reserved.
//

import UIKit
import AVFoundation

// variables extras

var engine:AVAudioEngine!
var playerNode:AVAudioPlayerNode!
var audioFile:AVAudioFile!
var mixer:AVAudioMixerNode!


// CARGA ARCHVIVO DE SONIDO Version 2

let myPath2 = NSBundle.mainBundle().pathForResource("03 Life for Rent", ofType: "mp3")

     //if let myPath2 = myPath2 {
    
    //let myfilepathURL = NSURL(fileURLWithPath: myPath2)
    
    //do {
      //  try audioFile = AVAudioFile(forReading: myfilepathURL)
   // }
        
    //catch {
      //  print("error")
//    }
//}


// inicializa objetos

//engine = AVAudioEngine()
//playerNode = AVAudioPlayerNode()
//audioFile = AVAudioFile()
//engine.attachNode(playerNode)
//mixer = engine.mainMixerNode
//mixer.outputVolume = 1.0
//mixer.pan = 0.0 // -1 to +1


// var error: NSError ?

//do {
  //  try engine.start()
//}
    
//catch let error1 as NSError {
//    error = error1
//    print("error couldn't start engine")
//    if let e = error {
//        print("error \(e.localizedDescription)")
 //   }
//}

// fin inicializa

//Funciones
// definicion del filtro EQualizer
var EQNode:AVAudioUnitEQ!
func addEQ() {
    EQNode = AVAudioUnitEQ(numberOfBands: 2)
    engine.attachNode(EQNode)
    
    var filterParams = EQNode.bands[0] as AVAudioUnitEQFilterParameters
    filterParams.filterType = .HighPass
    filterParams.frequency = 80.0
    
    filterParams = EQNode.bands[1] as AVAudioUnitEQFilterParameters
    filterParams.filterType = .Parametric
    filterParams.frequency = 500.0
    filterParams.bandwidth = 2.0
    filterParams.gain = 4.0
    
    //let format = mixer.outputFormatForBus(0)
    engine.connect(playerNode, to: EQNode, format: nil )
    engine.connect(EQNode, to: engine.mainMixerNode, format: nil)
    playerNode.play()
    //try engine.start()
    
}

// fin equalizer


