//
//  ViewController.swift
//  playerV1
//
//  Created by Alvar Arias on 15/12/15.
//  Copyright © 2015 Alvar Arias. All rights reserved.
//

import UIKit
import AVFoundation


class ViewController: UIViewController {

    // version para revisar
    var myplayer = AVAudioPlayer()
    
    
    // oulets
@IBOutlet weak var VolumenSliderControl: UISlider!
    
@IBOutlet weak var avanceLabel: UILabel!
    
@IBOutlet weak var volumenLabel: UILabel!
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //   CARGA ARCHVIVO DE SONIDO Version 1
        
     let myPath = NSBundle.mainBundle().pathForResource("03 Life for Rent", ofType: "mp3")
        
        if let myPath = myPath {
            
            let myfilepathURL = NSURL(fileURLWithPath: myPath)
            
            do {
                try myplayer = AVAudioPlayer(contentsOfURL: myfilepathURL)
             }
                
            catch {
                print("error")
            }
         }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    
    
    // Accion de los Botones
    
    var tiempo: Double = 0.0
    var vol: Float = 0.0

    @IBAction func BotonPausa(sender: AnyObject) {
        
        myplayer.pause()
        //playerNode.pause()
    }

    @IBAction func BotonPlay(sender: AnyObject) {
        
        myplayer.play()
        //playerNode.play()
        
        // intento de manejar el tiempo
        
    //var stringFromDoubleTiempo = String()
    //stringFromDoubleTiempo = "\(tiempo)"
    //avanceLabel.text = "\(tiempo)"
    // avanceLabel.text = ("\(myplayer.deviceCurrentTime)")
        
        
    }
    
    @IBAction func BotonStop(sender: AnyObject) {
        
        myplayer.stop()
        //playerNode.stop()
    }
    
    @IBAction func botonControlVolumen(sender: AnyObject) {
        
        myplayer.volume = VolumenSliderControl.value
        //playerNode.volume = VolumenSliderControl.value
        
        // muestra el volumen en pantalla
        
        vol = VolumenSliderControl.value
        volumenLabel.text = ("\(vol)")
    
    }
    
    @IBAction func botonFiltro(sender: AnyObject) {
        
        // Filtro EQ
        //addEQ()
        
    }
    
}

